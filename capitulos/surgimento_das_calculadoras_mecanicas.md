# Calculadoras Mecânicas

## Ábaco


O ábaco surgiu por volta de 5500.aC e por muito tempo foi o pricipal instrumento de auxilio para cálculo. O instrumento funciona por base de cálculo por decimais.

Um ábaco era feito normalmente de madeira, possuia formato retangular com hastes metálicas para segurar os elementos de contagem (que eram argolas, esferas, fichas, etc) que representava uma unidade de sua posição e em cada haste era representado uma posição digital (unidade, dezena...)

![foto ábaco](https://www.mentesliberadas.com/wp-content/uploads/2018/06/abaco-ruso-1024x717.jpg)

Porém, um ábaco não pode ser considerado uma calculadora mecânica visto que ele não é uma máquina, mas sim um instrumento de auxilio.

## Tentativas primitivas de uma calculadora mecânica

A primeira tentativa de fazer um instrumento que efetivamente realizasse cálculos aconteceu por volta do século XVII. Um dos primeiros dispositivos criados para essa finalidade foi chamado de "Bastões de Napier" ou "Barras de Napier". Criado pelo matemático escocês John Napier (1550-1617), o instrumento era formado por um conjunto de 9 bastões,um para cada dígito, que transformavam a multiplicação de dois números numa soma das tabuadas de cada dígito

![foto dos Bastões de Napier](http://2.bp.blogspot.com/-o64396pSyP8/Ut_4U__2lyI/AAAAAAAAAAs/Mo2YprkaJUI/s1600/Napier-barras.jpg)

O principio da primeira calculadora mecânica foi proposto em 1623 pelo alemão Wilhelm Shickard (1592-1635) que, ao colocá-lo em prática, construiu
o relógio calculador. O seu funcionamento era por rodas dentadas que representavam os algarismos de 0 a 9 e que, automaticamente, realizavam as quatro operações básicas. Essa máquina foi destruída por um incêndio, durante a Guerra dos 30 anos e as informações sobreviveram devido a uma carta enviada por Schickard a Kepler (1571-1630), conforme consta em Breton (1991).






## Fontes

[KALINKE, Marco Aurélio; MOCROSKY, Luciane; ESTEPHAN, Violeta Maria. Matemáticos, educadores matemáticos e tecnologias: uma articulação possível A relationship between history of technologies, mathematicians and mathematics educators. Educação Matemática Pesquisa: Revista do Programa de Estudos Pós-Graduados em Educação Matemática, v. 15, n. 2, 2013.]
