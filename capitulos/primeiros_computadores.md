# Primeiros computadores


## ENIAC 


O ENIAC (Eletronic Numerical Integrator and Computer), o primeiro computador eletrônico foi criado em 1946, com o objetivo de realizar  varios tipos de cálculos de artilharia para ajudar os aliados durante a Segunda Guerra Mundial, porém ele foi terminado 3 meses após a guerra e então usado na Guerra Fria.

![1946  - ENIAC](https://sites.google.com/site/historiasobreossitesdebusca/_/rsrc/1272389768645/primeiro-computador-do-mundo/Eniac%20foto.jpg) 

O primeiro computador era  extremamente gigante tinha 30 toneladas e ocupava uma área de 180 m². Sua produção custou nada menos do que US$ 500 mil na época, o que hoje representaria aproximadamente US$ 6 milhões






## FONTES 

https://www.cnnbrasil.com.br/tecnologia/2021/02/23/do-eniac-ao-notebook-confira-a-evolucao-dos-computadores-nas-ultimas-decadas
     https://www.hardware.com.br/termos/eniac
