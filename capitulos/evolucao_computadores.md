# Evolução dos computadores pessoais

O primeiro computador pessoal inventado foi o Apple I, em 1976 pelos americanos Steve Jobs e Stephan Wozniak. o Apple I utilizava o sistema operacional BASIC Applesoft, um processador MOS 6502 de 1,00 MHz e memória de 4 KiB (base) expansivel até 48KiB. 

![APPLE I](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Apple_I_Computer.jpg/1200px-Apple_I_Computer.jpg)

Em 1981, a IBM lançou o seu primeiro PC (Personal Computer), que se tornou um sucesso comercial. O sistema operacional utilizado era o MS-DOS, desenvolvido pela empresa de softwares Microsoft.

![IBM PC](https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/IBM_PC_5150.jpg/280px-IBM_PC_5150.jpg)

Na época o unico computador pessoal  a fazer frente aos PCs é o Macintosh, lançado em 1984, que revolucionou o mercado ao promover o uso de ícones e do mouse. Em 1985 a Microsoft lança a interface gráfica Windows, adaptando para os PCs o uso de ícones e do mouse, mas so começou a fazer sucesso a partir dos anos 90, com o lançamento da versão 3.0.

![Macintosh](https://veja.abril.com.br/wp-content/uploads/2016/05/vida-digital-apple-mac-30-anos-20140124-013-original.jpeg)

Na década de 90 surgiram os computadores que, além do processamento de dados, reúnem fax, modem, secretária eletrônica, scanner, acesso à Internet e drive para CD-ROM. Os computadores portáteis (laptops e palmtops), marcas da miniaturização da tecnologia, também se popularizam nos anos 90.
