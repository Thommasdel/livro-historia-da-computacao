# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

# Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/evolucao_computadores.md)
    - [Primeira Geração]()
1. [Computação Móvel](capitulos/computacao_movel.md)
1. [Futuro](capitulos/futuro.md)



## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![]()  | Thomas Delfs             | Thommasdel       | [thomas.delfs2@hotmail.com](mailto:thomas.delfs2@hotmail.com)
| ![]()  | Leonardo Girardi         | LeonardoGirardi  | [leonardogirardileal123@gmail.com](mailto:leonardogirardileal123@gmail.com)      
| ![https://i.imgur.com/58S7Kom.jpg](https://i.imgur.com/58S7Kom.jpg)  | Marcelo Campos Lamounier | marcelolamounier | [marcelolamounier@alunos.utfpr.edu.br](mailto:marcelolamounier@alunos.utfpr.edu.br)      
| ![]()  | Natália Gonçalves Ramos  | nataliagramos    | [natflyer1998@gmail.com](mailto:)        
